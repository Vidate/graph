using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace GraphProgram
{
    interface IEdge
    {
         string PathData { get; set; }

    }
    public class Edge : IEdge
    {
        private string _pathData;

        public string PathData
        {
            get { return _pathData; }
            set { _pathData = value; }
        }
        public Edge(string path)
        {
            PathData = path;
        }
    }
}
