﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace GraphProgram
{

    public interface INode
    {
        Thickness Margin { get; set; }
        int Number { get; set; }
    }
    public class Node : INode
    {
        private Thickness _margin;
        private int _number;

        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }
        public Thickness Margin
        {
            get { return _margin; }
            set { _margin = value; }
        }
        public Node(Thickness margin, int number)
        {
            Number = number;
            Margin = margin;
        }
    }
}