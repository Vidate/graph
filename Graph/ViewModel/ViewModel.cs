﻿using Commander;
using GraphProgram.Command;
using GraphProgram.Converters;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GraphProgram
{
    [ImplementPropertyChanged]
    class ViewModel
    {
        private int _nodeSize;
        private double _fontNodeSize;
        private int _edgeSize;
        private int _numberOfnode;
        private int _numberOfedge;
        private int _actualSizeWidth;
        private int _actualSizeHeight;
        private int _radius;

        private string _directoryFirstGraph;
        private string _directorySecondGraph;

        private int[][] _matrixSum;
        private int[][] _matrixDif;
        private int[][] _matrixFirst;
        private int[][] _matrixSecond;

        private IEdge _edgeFirstGraph;
        private IEdge _edgeSecondGraph;
        private IEdge _edgeDifGraph;
        private IEdge _edgeSumGraph;

        private ObservableCollection<INode> _nodeGraph;

        public ViewModel()
        {
            NodeSize = 30;
            FontNodeSize = 10;
            EdgeSize = 1;
            Radius = 75;
        }

        public ObservableCollection<INode> NodeGraph
        {
            get { return _nodeGraph; }
            set { _nodeGraph = value; }
        }
        public int EdgeSize
        {
            get { return _edgeSize; }
            set { _edgeSize = value; }
        }
        public double FontNodeSize
        {
            get { return _fontNodeSize; }
            set { _fontNodeSize = value; }
        }
        public int NodeSize
        {
            get { return _nodeSize; }
            set
            {
                _nodeSize = value;
            }
        }
        public int NumberOfnode
        {
            get { return _numberOfnode; }
            set { _numberOfnode = value; }
        }
        public int NumberOfedge
        {
            get { return _numberOfedge; }
            set { _numberOfedge = value; }
        }
        public int ActualSizeWidth
        {
            get { return _actualSizeWidth; }
            set { _actualSizeWidth = value; }
        }
        public int ActualSizeHeight
        {
            get { return _actualSizeHeight; }
            set { _actualSizeHeight = value; }
        }
        public int[][] MatrixFirst
        {
            get { return _matrixFirst; }
            set { _matrixFirst = value; }
        }
        public int[][] MatrixSecond
        {
            get { return _matrixSecond; }
            set { _matrixSecond = value; }
        }
        public int[][] MatrixDif
        {
            get { return _matrixDif; }
            set { _matrixDif = value; }
        }
        public int[][] MatrixSum
        {
            get { return _matrixSum; }
            set { _matrixSum = value; }
        }

        public string DirectoryFirstGraph
        {
            get { return _directoryFirstGraph; }
            set { _directoryFirstGraph = value; }
        }
        public string DirectorySecondGraph
        {
            get { return _directorySecondGraph; }
            set { _directorySecondGraph = value; }
        }

        public IEdge EdgeFirstGraph
        {
            get { return _edgeFirstGraph; }
            set { _edgeFirstGraph = value; }
        }
        public IEdge EdgeSecondGraph
        {
            get { return _edgeSecondGraph; }
            set { _edgeSecondGraph = value; }
        }
        public IEdge EdgeDifGraph
        {
            get { return _edgeDifGraph; }
            set { _edgeDifGraph = value; }
        }
        public IEdge EdgeSumGraph
        {
            get { return _edgeSumGraph; }
            set { _edgeSumGraph = value; }
        }

        public ICommand LoadFiles
        {
            get { return new RelayCommand(loadFiles, x => true); }
        }
        public ICommand SelectFirstPath
        {
            get { return new RelayCommand(selectFirstPath, x => true); }
        }
        public ICommand SelectSecondPath
        {
            get { return new RelayCommand(selectSecondPath, x => true); }
        }
        public ICommand Random
        {
            get { return new RelayCommand(random, x => true); }
        }
        public ICommand UpdateNode
        {
            get { return new RelayCommand(updateNode, x => true); }
        }

        public int Radius
        {
            get { return _radius; }
            set {
                _radius = value;
                try
                {
                    RefreshEdge();
                    RefreshNode();
                }
                catch (Exception)
                {

                   
                }
            }
        }

        private void random(object obj)
        {
            GenerateRandomNode();
            MatrixFirst = GenerateRandomMatrix();
            MatrixSecond = GenerateRandomMatrix();
            MatrixSum = Add(MatrixFirst, MatrixSecond);
            MatrixDif = Substruct(MatrixFirst, MatrixSecond);
            RefreshEdge();
            RefreshNode();
        }
        private void updateNode(object obj)
        {
            Node node = (Node)obj;
            if (node != null && node.Number >= 0)
            {
                NodeGraph.Remove(NodeGraph.Where(x => x.Number == node.Number).FirstOrDefault());
                var tmp = new Node(new Thickness(
                    node.Margin.Left, node.Margin.Top, 0, 0), node.Number);
                NodeGraph.Add(tmp);
                RefreshEdge();
                RefreshNode();
            }
        }
        private void selectFirstPath(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                DirectoryFirstGraph = openFileDialog.FileName;
        }
        private void selectSecondPath(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                DirectorySecondGraph = openFileDialog.FileName;
        }
        private void loadFiles(object obj)
        {
            Random random = new Random();
            NodeGraph = new ObservableCollection<INode>();
            if (DirectorySecondGraph != null && DirectoryFirstGraph != null)
            {
                MatrixFirst = LoadMatrix(DirectoryFirstGraph);
                MatrixSecond = LoadMatrix(DirectorySecondGraph);
                NumberOfnode = MatrixFirst.GetLength(0);
                double angle = 30;
                for (int i = 0; i < NumberOfnode; i++)
                {
                    //NodeGraph.Add(new Node(new Thickness(
                    //          random.Next(-ActualSizeWidth + (NodeSize), ActualSizeWidth - (NodeSize)),
                    //          random.Next(-ActualSizeHeight + (NodeSize), ActualSizeHeight - (NodeSize)), 0, 0), i));
                    int Y = Convert.ToInt32(Radius * Math.Sin(angle));
                    int X = Convert.ToInt32(Radius * Math.Cos(angle));
                    NodeGraph.Add(new Node(new Thickness(X, Y, 0, 0), i));
                    angle += (2 * Math.PI) / NumberOfnode;
                }
                MatrixSum = Add(MatrixFirst, MatrixSecond);
                MatrixDif = Substruct(MatrixFirst, MatrixSecond);
                RefreshEdge();
                RefreshNode();
            }
        }
        private bool canRandom(object obj)
        {
            if (NumberOfedge <= 0 && NumberOfedge > NumberOfnode)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void RefreshNode()
        {
            var tmp = new ObservableCollection<INode>();
            foreach (var item in NodeGraph)
            {
                tmp.Add(item);
            }
            foreach (var item in tmp)
            {
                NodeGraph.Remove(item);
            }
            NodeGraph = new ObservableCollection<INode>();
            foreach (var item in tmp)
            {
                NodeGraph.Add(item);
            }
        }
        private void RefreshEdge()
        {
            EdgeFirstGraph = new Edge(GeneratePathString(MatrixFirst));
            EdgeSecondGraph = new Edge(GeneratePathString(MatrixSecond));
            EdgeDifGraph = new Edge(GeneratePathString(MatrixDif));
            EdgeSumGraph = new Edge(GeneratePathString(MatrixSum));
        }
        private string GeneratePathString(int[][] matrix)
        {
            string result = "";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                // liczba wierzcholkow jest rowniez liczba krawedzi ja cos poprawic
                for (int j = 0; j < matrix[i].GetLength(0); j++)
                {
                    if (matrix[i][j] == 1)
                    {
                        var n = NodeGraph.Where(x => x.Number == i).FirstOrDefault().Margin;
                        var m = NodeGraph.Where(x => x.Number == j).FirstOrDefault().Margin;
                        result += " M "
                            + ((int)(ActualSizeWidth / 2 + m.Left / 2)).ToString()
                            + "," + ((int)(ActualSizeHeight / 2 + m.Top / 2)).ToString()
                            + " L " + ((int)(ActualSizeWidth / 2 + n.Left / 2)).ToString()
                            + "," + ((int)(ActualSizeHeight / 2 + n.Top / 2)).ToString();
                    }
                }
            }
            return result;
        }
        private void GenerateRandomNode()
        {
            NodeGraph = new ObservableCollection<INode>();
            double angle = 1;
            for (int i = 0; i < NumberOfnode; i++)
            {
                int Y = Convert.ToInt32(Radius * Math.Sin(angle));
                int X = Convert.ToInt32(Radius * Math.Cos(angle));
                NodeGraph.Add(new Node(new Thickness(X, Y, 0, 0), i));
                angle += (2 * Math.PI) / NumberOfnode;
                //NodeGraph.Add(new Node(new Thickness(
                //    random.Next(-ActualSizeWidth + NodeSize, ActualSizeWidth - NodeSize)
                //    , random.Next(-ActualSizeHeight + NodeSize, ActualSizeHeight - NodeSize)
                //    , 0, 0), i));

            }
        }
        private int[][] GenerateRandomMatrix()
        {
            int[][] resultRandomMatrix;
            resultRandomMatrix = new int[NumberOfnode][];
            for (int i = 0; i < NumberOfnode; i++)
            {
                resultRandomMatrix[i] = new int[NumberOfnode];
                for (int j = 0; j < NumberOfnode; j++)
                {
                    resultRandomMatrix[i][j] = 0;
                }
            }
            int count = 0;

            if (NumberOfedge <= GetSumByNumberOfNode() && NumberOfedge > 0)
            {
                Random random = new Random(DateTime.Now.Millisecond);
                Random random2 = new Random(DateTime.Now.Millisecond+5);

                do
                {
                    int firstRand = Convert.ToInt32(random.Next(0, NumberOfnode));
                    int secondRand = Convert.ToInt32(random2.Next(0, NumberOfnode));
                    if (resultRandomMatrix[firstRand][secondRand] == 0 && firstRand != secondRand && resultRandomMatrix[secondRand][firstRand] == 0)
                    {
                        resultRandomMatrix[firstRand][secondRand] = 1;
                        count++;
                    }
                } while (!(count == NumberOfedge));
            }
            return resultRandomMatrix;
        }
        private int[][] LoadMatrix(string path)
        {
            int[][] result;
            var tmp = File.ReadAllLines(path).Select(x => x.Split(',')).ToArray();
            result = tmp.Select(x => x.Select(y => Convert.ToInt32(y)).ToArray()).ToArray();
            return result;
        }

        private double GetSumByNumberOfNode()
        {
            int sum = 0;

            for (int s = 2; s < NumberOfnode + 1; s++)
            {
                sum += s - 1;
            }
            return sum;
        }
        public int[][] Add(int[][] graphFirst, int[][] graphSecond)
        {


            if (graphSecond.GetLength(0) > graphFirst.GetLength(0))
            {
                var tmpToRe = new int[graphSecond.GetLength(0)][];
                for (int i = 0; i < tmpToRe.GetLength(0); i++)
                {
                    tmpToRe[i] = new int[graphSecond[i].GetLength(0)];

                    for (int j = 0; j < tmpToRe[i].GetLength(0); j++)
                    {
                        if (graphFirst.GetLength(0) > i)
                        {
                            tmpToRe[i][j] = graphFirst[i][j];
                        }
                        else
                        {
                            tmpToRe[i][j] = 0;
                        }
                    }
                }
                graphFirst = tmpToRe;
            }
            else if (graphSecond.GetLength(0) < graphFirst.GetLength(0))
            {
                var tmpToRe = new int[graphFirst.GetLength(0)][];
                for (int i = 0; i < tmpToRe.GetLength(0); i++)
                {
                    tmpToRe[i] = new int[graphFirst[i].GetLength(0)];

                    for (int j = 0; j < tmpToRe[i].GetLength(0); j++)
                    {
                        if (graphSecond.GetLength(0) > i)
                        {
                            tmpToRe[i][j] = graphSecond[i][j];
                        }
                        else
                        {
                            tmpToRe[i][j] = 0;
                        }   
                    }
                }
                graphSecond = tmpToRe;
            }

            var resutlMatrix = new int[graphSecond.GetLength(0)][];
            for (int i = 0; i < resutlMatrix.GetLength(0); i++)
            {
                resutlMatrix[i] = new int[graphSecond[i].GetLength(0)];
            }

            for (int i = 0; i < resutlMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < resutlMatrix[i].GetLength(0); j++)
                {
                    if (graphSecond[i][j] + graphFirst[i][j] >= 1)
                    {
                        resutlMatrix[i][j] = 1;
                    }
                    else
                    {
                        resutlMatrix[i][j] = 0;

                    }
                }
            }
            return resutlMatrix;
        }
        public int[][] Substruct(int[][] graphFirst, int[][] graphSecond)
        {
            var differenceOfMatrixes = new int[graphSecond.GetLength(0)][];
            for (int i = 0; i < differenceOfMatrixes.GetLength(0); i++)
            {
                differenceOfMatrixes[i] = new int[graphSecond[i].GetLength(0)];
            }

            for (int i = 0; i < differenceOfMatrixes.GetLength(0); i++)
            {
                for (int j = 0; j < differenceOfMatrixes[i].GetLength(0); j++)
                {
                    if (graphSecond[i][j] - graphFirst[i][j] <= 0)
                    {
                        differenceOfMatrixes[i][j] = 0;
                    }
                    else if (graphSecond[i][j] - graphFirst[i][j] >= 1)
                    {
                        differenceOfMatrixes[i][j] = 1;
                    }
                }
            }

            return differenceOfMatrixes;
        }
    }
}
