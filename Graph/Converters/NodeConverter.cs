﻿using GraphProgram;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace GraphProgram.Converters
{
    public class ContainerNode
    {
        private Thickness _margin;
        private Button _button;
        private int _number;
        public Thickness Margin
        {
            get { return _margin; }
            set { _margin = value; }
        }
        public Button Button
        {
            get { return _button; }
            set { _button = value; }
        }
        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }
        public ContainerNode(int nr, Thickness mar, Button but)
        {
            _button = but;
            _number = nr;
            _margin = mar;
        }
    }
    public class NodeConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var a = (Thickness)value[0];
            int b = (int)value[1];
            return new Node(a, b);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
