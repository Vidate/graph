﻿using MahApps.Metro.Controls;
using System;
using GraphProgram;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace GraphProgram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        ViewModel vm;
        public MainWindow()
        {
            vm = (ViewModel)base.DataContext;
            InitializeComponent();
        }
        private bool lockButton = false;
        private void PreviewMouseDownEvent(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                //File.AppendAllText(@"C:\XYZ\err.txt", "Mouse down\r\n");
                lockButton = true;
            }
        }
        private void PreviewMouseUpEvent(object sender, MouseButtonEventArgs e)
        {
            lockButton = false;
            // File.AppendAllText(@"C:\XYZ\err.txt", "Mouse Up\r\n");
        }

        private void PreviewMouseMoveEvent(object sender, MouseEventArgs e)
        {
            if (lockButton)
            {
                var button = (sender as Button);
                var prent = (sender as Button).TryFindParent<ItemsControl>();
                double height = (prent).ActualHeight;
                double width = (prent).ActualWidth;
                Point mouse = Mouse.GetPosition(prent);
                button.Margin = new Thickness(
                    (mouse.X * 2 - width),
                    (mouse.Y * 2 - height),
                    button.Margin.Right, button.Margin.Bottom);
                //File.AppendAllText(@"C:\XYZ\err.txt","zmiana polozenia buttona x:"
                //    + (mouse.X * 2 - width) +
                //    " y:" + (mouse.Y * 2 - height) + "\r\n");
            }
        }
    }
}
